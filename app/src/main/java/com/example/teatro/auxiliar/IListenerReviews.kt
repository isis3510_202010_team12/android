package com.example.teatro.auxiliar

import com.example.teatro.data.review.Review

interface IListenerReviews {
    fun onClick(obra: Review)
}