package com.example.teatro.auxiliar

import com.example.teatro.data.obra.Obra

interface IListener {
    fun onClick(obra: Obra)
}