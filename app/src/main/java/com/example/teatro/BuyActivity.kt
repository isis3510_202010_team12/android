package com.example.teatro

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.teatro.data.funcion.Funcion
import com.example.teatro.data.funcion.FuncionViewModel
import com.example.teatro.data.obra.Obra
import com.example.teatro.data.obra.ObraViewModel
import com.example.teatro.data.review.Review
import com.example.teatro.data.review.ReviewViewModel
import com.example.teatro.data.teatro.Teatro
import com.example.teatro.data.teatro.TeatroViewModel
import com.example.teatro.data.user.UserViewModel
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.firestore.FirebaseFirestore
import com.squareup.picasso.Picasso
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class BuyActivity : AppCompatActivity() {

    private lateinit var userViewModel: UserViewModel
    private lateinit var obraViewModel: ObraViewModel
    private lateinit var teatroViewModel: TeatroViewModel
    private lateinit var reviewViewModel: ReviewViewModel
    private lateinit var funcionViewModel: FuncionViewModel
    private lateinit var etNumber: TextView
    private lateinit var valor: TextView
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    var min = 0

    private lateinit var sharedPreferences: SharedPreferences

    private inner class AddHandler(val diff: Int) : View.OnClickListener {
        override fun onClick(v: View?) {
            var newValue = value + diff
            if (newValue < min) {
                newValue = min
            }
            valor.text = "${newValue * 10000} \$"
            etNumber.text = newValue.toString()

        }
    }

    var value: Int
        get() {
            try {
                val value = etNumber.text.toString()
                return value.toInt()
            } catch (ex: Exception) {
                Log.e("com.example.teatro.auxiliar.HorizontalNumberPicker", ex.toString())
            }
            return 0
        }
        set(value) {
            etNumber.text = value.toString()
        }

    override fun onCreate(savedInstanceState: Bundle?) {

        userViewModel = ViewModelProvider(this).get(UserViewModel::class.java)
        obraViewModel = ViewModelProvider(this).get(ObraViewModel::class.java)
        teatroViewModel = ViewModelProvider(this).get(TeatroViewModel::class.java)
        reviewViewModel = ViewModelProvider(this).get(ReviewViewModel::class.java)
        funcionViewModel = ViewModelProvider(this).get(FuncionViewModel::class.java)
        sharedPreferences =
            getSharedPreferences(getString(R.string.credentials), Context.MODE_PRIVATE)

        firebaseAnalytics = FirebaseAnalytics.getInstance(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comprar)
        val obra = intent.extras?.get("name") as String
        supportActionBar!!.title = obra

        Picasso.get().load(intent.extras?.get("image") as String)
            .into(findViewById<ImageView>(R.id.item_image))

        findViewById<TextView>(R.id.name).text = obra
        findViewById<TextView>(R.id.year).text = intent.extras?.get("calificacion") as String
        findViewById<TextView>(R.id.title).text = intent.extras?.get("descripcion") as String

        funcionViewModel.apply { string = obra }

        funcionViewModel.str()

        val spinner: Spinner = findViewById(R.id.date_field)

        val list: ArrayList<String> = ArrayList()

        val dataAdapter: ArrayAdapter<String> = ArrayAdapter<String>(
            this,
            android.R.layout.simple_spinner_item, list
        )
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = dataAdapter

        valor = findViewById(R.id.value_field)

        valor.text = "0\$"

        etNumber = findViewById(R.id.et_quantity)
        val btnLess: Button = findViewById(R.id.btn_less)
        btnLess.setOnClickListener(AddHandler(-1))
        val btnMore: Button = findViewById(R.id.btn_more)
        btnMore.setOnClickListener(AddHandler(1))

        funcionViewModel.estaObra.observe(
            this,
            Observer { t ->
                dataAdapter.clear()
                t.forEach {
                    dataAdapter.add(it.fecha)
                }
            })
        findViewById<Button>(R.id.button2).setOnClickListener {
            GlobalScope.launch {
                withContext(Dispatchers.IO) {
                    val db = FirebaseFirestore.getInstance()
                    db.collection("horarios").whereEqualTo("horario", spinner.selectedItem)
                        .whereEqualTo("obra", obra).get().addOnSuccessListener { x ->
                            x.documents.forEach { y ->
                                val seats = Integer.parseInt("" + y["asientosDisponibles"] as Long) - value
                                if (seats >= 0) {
                                    db.collection("horarios").document(y.id)
                                        .update("asientosDisponibles", seats)
                                    val bundle = Bundle()
                                    bundle.putString(
                                        FirebaseAnalytics.Param.CONTENT,
                                        "Boletas compradas"
                                    )
                                    firebaseAnalytics.logEvent(
                                        FirebaseAnalytics.Event.ECOMMERCE_PURCHASE,
                                        bundle
                                    )
                                    db.collection("reviews").add(
                                        hashMapOf(
                                            "nameObra" to obra,
                                            "emailUser" to
                                                    sharedPreferences.getString("user", null),
                                            "value" to -1,
                                            "teatro" to intent.extras?.get("teatro") as String
                                        )
                                    )
                                    // loadData()
                                    finish()
                                }
                            }
                        }
                    finish()
                }

            }

        }
    }

    private fun loadData() {
        GlobalScope.launch {

            withContext(Dispatchers.IO) {

                val db = FirebaseFirestore.getInstance()

                val sharedPreferences =
                    getSharedPreferences(getString(R.string.credentials), Context.MODE_PRIVATE)

                val email = sharedPreferences.getString("user", null)

                val revs = reviewViewModel.pending()

                revs.forEach { xd ->
                    val bundle = Bundle()
                    firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SHARE, bundle)

                    val desd: ArrayList<Double> = ArrayList()
                    db.collection("reviews").whereEqualTo("nameObra", xd.nameObra)
                        .whereEqualTo("emailUser", xd.emailUser).get()
                        .addOnSuccessListener { result ->
                            result.forEach {
                                db.collection("reviews").document(it.id)
                                    .update("value", xd.value).addOnSuccessListener {
                                        reviewViewModel.update(
                                            Review(
                                                xd.nameObra,
                                                xd.emailUser,
                                                xd.value,
                                                xd.teatro,
                                                false
                                            )
                                        )
                                        db.collection("reviews")
                                            .whereEqualTo("nameObra", xd.nameObra).get()
                                            .addOnSuccessListener { resultSet ->
                                                resultSet.forEach { result ->
                                                    try {
                                                        desd.add(result["value"] as Double)
                                                    } catch (e: java.lang.Exception) {
                                                        //do nothing
                                                    }
                                                }
                                                db.collection("obras").document(xd.nameObra)
                                                    .update("score", desd.average())
                                                    .addOnSuccessListener {
                                                        Log.d("LOGRADO", "" + desd.average())
                                                    }.addOnFailureListener {
                                                        Log.e("SET", "failed")
                                                    }
                                            }.addOnFailureListener {
                                                Log.e("SET", "failed")
                                            }
                                    }
                            }
                        }.addOnFailureListener {
                            Log.e("SET", "failed")
                        }
                }

                //Limpiar y obtener reviews
                reviewViewModel.deleteAll()
                db.collection("reviews").whereEqualTo("emailUser", email)
                    .get()
                    .addOnSuccessListener { result ->
                        for (document in result) {
                            reviewViewModel.insert(
                                Review(
                                    nameObra = document["nameObra"] as String,
                                    emailUser = document["emailUser"] as String,
                                    value = ("" + document["value"]).toDouble(),
                                    teatro = document["teatro"] as String,
                                    pending = false
                                )
                            )
                        }
                    }
                    .addOnFailureListener { exception ->
                        Log.w("COLLECTION", "Error getting documents.", exception)
                    }

                //Limpiar y obtener obras
                obraViewModel.deleteAll()
                db.collection("obras")
                    .get()
                    .addOnSuccessListener { result ->
                        for (document in result) {
                            obraViewModel.insert(
                                Obra(
                                    name = document.id,
                                    image = document.data["image"] as String,
                                    score = ("" + document.data["score"]).toDouble(),
                                    description = document.data["description"] as String,
                                    lugar = document.data["place"] as String,
                                    escena = document.data["escena"] as String
                                )
                            )
                        }
                    }
                    .addOnFailureListener { exception ->
                        Log.w("COLLECTION", "Error getting documents.", exception)
                    }

                //Limpiar y obtener teatros
                teatroViewModel.deleteAll()
                db.collection("teatros")
                    .get()
                    .addOnSuccessListener { result ->
                        for (document in result) {
                            teatroViewModel.insert(
                                Teatro(
                                    name = document.id,
                                    latitude = document["latitude"] as Double,
                                    longitude = document["longitude"] as Double
                                )
                            )
                        }
                    }
                    .addOnFailureListener { exception ->
                        Log.w("COLLECTION", "Error getting documents.", exception)
                    }

                //Limpiar y obtener funciones
                funcionViewModel.deleteAll()
                db.collection("horarios")
                    .get()
                    .addOnSuccessListener { result ->
                        for (document in result) {
                            funcionViewModel.insert(
                                Funcion(
                                    id = document.id,
                                    asientosDisponibles = Integer.parseInt("" + document["asientosDisponibles"] as Long),
                                    fecha = document["horario"] as String,
                                    obra = document["obra"] as String
                                )
                            )
                        }
                    }
                    .addOnFailureListener { exception ->
                        Log.w("COLLECTION", "Error getting documents.", exception)
                    }
            }
        }
    }
}