package com.example.teatro.data.user

import androidx.lifecycle.LiveData

class UserRepository(private val teatroDao: UserDao) {

    val allWords: LiveData<User> = teatroDao.getUser()

    suspend fun removeAll() {
        teatroDao.deleteAll()
    }

    suspend fun insert(teatro: User) {
        teatroDao.insert(teatro)
    }
}