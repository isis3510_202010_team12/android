package com.example.teatro.data.obra

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface ObraDao {

    @Query("SELECT * from obra_table ORDER BY name ASC")
    fun getObras(): LiveData<List<Obra>>

    @Query("SELECT * from obra_table WHERE place = :string ORDER BY name ASC")
    fun getObrasByTeatro(string: String): LiveData<List<Obra>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(obra: Obra)

    @Query("SELECT * from obra_table WHERE name = :string LIMIT 1")
    fun getOne(string: String) : Obra

    @Query("DELETE FROM obra_table")
    suspend fun deleteAll()
}