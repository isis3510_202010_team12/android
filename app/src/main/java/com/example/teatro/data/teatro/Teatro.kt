package com.example.teatro.data.teatro

import androidx.room.*

@Entity(tableName = "teatro_table")
data class Teatro(
    @PrimaryKey @ColumnInfo(name = "name") val name: String
    , @ColumnInfo(name = "latitude") val latitude: Double
    , @ColumnInfo(name = "longitude") val longitude: Double
)