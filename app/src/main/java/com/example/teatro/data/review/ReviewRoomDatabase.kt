package com.example.teatro.data.review

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Review::class], version = 1, exportSchema = false)
abstract class ReviewRoomDatabase : RoomDatabase() {

    abstract fun reviewDao(): ReviewDao

    companion object {
        @Volatile
        private var INSTANCE: ReviewRoomDatabase? = null

        fun getDatabase(
            context: Context
        ): ReviewRoomDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ReviewRoomDatabase::class.java,
                    "reviews1_database"
                )
                    .build()
                INSTANCE = instance
                instance
            }
        }
    }
}
