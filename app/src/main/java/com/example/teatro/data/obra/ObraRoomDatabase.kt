package com.example.teatro.data.obra

import android.content.Context
import androidx.room.*
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Database(entities = [Obra::class], version = 1, exportSchema = false)
abstract class ObraRoomDatabase : RoomDatabase() {

    abstract fun obraDao(): ObraDao

    companion object {
        @Volatile
        private var INSTANCE: ObraRoomDatabase? = null

        fun getDatabase(
            context: Context
        ): ObraRoomDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ObraRoomDatabase::class.java,
                    "obra_database"
                )
                    .build()
                INSTANCE = instance
                instance
            }
        }
    }
}
