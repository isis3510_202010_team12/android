package com.example.teatro.data.user

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class UserViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: UserRepository
    val users: LiveData<User>

    init {
        val userDao = UserRoomDatabase.getDatabase(application).userDao()
        repository = UserRepository(userDao)
        users = repository.allWords
    }

    fun reset() = viewModelScope.launch(Dispatchers.IO) {
        repository.removeAll()
    }
    
    fun insert(obra: User) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(obra)
    }
}