package com.example.teatro.data.teatro

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TeatroViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: TeatroRepository
    val teatros: LiveData<List<Teatro>>

    init {
        val teatroDao = TeatroRoomDatabase.getDatabase(application).teatroDao()
        repository = TeatroRepository(teatroDao)
        teatros = repository.allWords
    }

    fun deleteAll() = viewModelScope.launch(Dispatchers.IO) {
        repository.deleteAll()
    }

    fun insert(obra: Teatro) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(obra)
    }
}