package com.example.teatro.data.obra

import androidx.room.*

@Entity(tableName = "obra_table")
data class Obra(
    @PrimaryKey @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "description") val description: String,
    @ColumnInfo(name = "escena") val escena: String,
    @ColumnInfo(name = "place") val lugar: String,
    @ColumnInfo(name = "score") val score: Double,
    @ColumnInfo(name = "image") val image: String
)