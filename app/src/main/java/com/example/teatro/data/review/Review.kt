package com.example.teatro.data.review

import androidx.room.*

@Entity(tableName = "review_table", primaryKeys = ["nameObra", "emailUser"])
data class Review(
    @ColumnInfo(name = "nameObra") val nameObra: String,
    @ColumnInfo(name = "emailUser") val emailUser: String,
    @ColumnInfo(name = "value") val value: Double,
    @ColumnInfo(name = "teatro") val teatro: String,
    @ColumnInfo(name = "pending") val pending: Boolean
)