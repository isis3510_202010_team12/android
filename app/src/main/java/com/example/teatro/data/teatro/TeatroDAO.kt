package com.example.teatro.data.teatro

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface TeatroDao {

    @Query("SELECT * from teatro_table ORDER BY name ASC")
    fun getTeatros(): LiveData<List<Teatro>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(teatro: Teatro)

    @Query("DELETE FROM teatro_table")
    suspend fun deleteAll()
}