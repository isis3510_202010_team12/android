package com.example.teatro.data.review

import androidx.lifecycle.LiveData

class ReviewRepository(private val reviewDao: ReviewDao) {

    val reviews: LiveData<List<Review>> = reviewDao.getReviews()

    fun pending() : List<Review>{
        return reviewDao.getPending()
    }

    suspend fun deleteAll(){
        reviewDao.deleteAll()
    }

    suspend fun insert(review: Review) {
        reviewDao.insert(review)
    }

    suspend fun update(review: Review) {
        reviewDao.update("" + review.value, review.nameObra, review.emailUser, if(review.pending) 1 else 0)
    }
}