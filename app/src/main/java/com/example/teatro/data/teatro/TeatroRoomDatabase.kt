package com.example.teatro.data.teatro

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Teatro::class], version = 1, exportSchema = false)
abstract class TeatroRoomDatabase : RoomDatabase() {

    abstract fun teatroDao(): TeatroDao

    companion object {
        @Volatile
        private var INSTANCE: TeatroRoomDatabase? = null

        fun getDatabase(
            context: Context
        ): TeatroRoomDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    TeatroRoomDatabase::class.java,
                    "teatro_database"
                )
                    .build()
                INSTANCE = instance
                instance
            }
        }
    }
}
