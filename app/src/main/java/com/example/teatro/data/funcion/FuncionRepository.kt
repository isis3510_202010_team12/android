package com.example.teatro.data.funcion

import androidx.lifecycle.LiveData

class FuncionRepository(private val wordDao: FuncionDao) {

    val allWords: LiveData<List<Funcion>> = wordDao.getFunciones()

    fun estaObra(string: String): LiveData<List<Funcion>> {
        return wordDao.getFuncionesByObra(string)
    }

    suspend fun insert(funcion: Funcion) {
        wordDao.insert(funcion)
    }


    suspend fun deleteAll() {
        wordDao.deleteAll()
    }
}