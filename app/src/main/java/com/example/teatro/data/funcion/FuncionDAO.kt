package com.example.teatro.data.funcion

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface FuncionDao {

    @Query("SELECT * from funcion_table ORDER BY obra ASC")
    fun getFunciones(): LiveData<List<Funcion>>

    @Query("SELECT * from funcion_table WHERE obra = :string")
    fun getFuncionesByObra(string: String): LiveData<List<Funcion>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(funcion: Funcion)

    @Query("DELETE FROM funcion_table")
    suspend fun deleteAll()
}