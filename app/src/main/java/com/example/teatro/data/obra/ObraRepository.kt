package com.example.teatro.data.obra

import androidx.lifecycle.LiveData

class ObraRepository(private val wordDao: ObraDao) {

    val allWords: LiveData<List<Obra>> = wordDao.getObras()

    fun esteTeatro(string: String): LiveData<List<Obra>> {
        return wordDao.getObrasByTeatro(string)
    }

    suspend fun insert(obra: Obra) {
        wordDao.insert(obra)
    }

    fun getOne(string: String) : Obra {
        return wordDao.getOne(string)
    }

    suspend fun deleteAll() {
        wordDao.deleteAll()
    }
}