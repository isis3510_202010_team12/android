package com.example.teatro.data.review

import androidx.lifecycle.LiveData
import androidx.room.*
import java.nio.channels.AcceptPendingException

@Dao
interface ReviewDao {

    @Query("SELECT * from review_table ORDER BY nameObra ASC")
    fun getReviews(): LiveData<List<Review>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(review: Review)

    @Query("DELETE FROM review_table")
    suspend fun deleteAll()

    @Query("UPDATE review_table SET value = :value, pending = :pending WHERE nameObra = :obra AND emailUser = :email")
    suspend fun update(value: String, obra: String, email: String, pending : Int)

    @Query("SELECT * from review_table WHERE pending = 1 ORDER BY nameObra ASC")
    fun getPending():List<Review>
}