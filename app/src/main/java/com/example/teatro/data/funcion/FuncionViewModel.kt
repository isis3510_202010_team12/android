package com.example.teatro.data.funcion

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FuncionViewModel(application: Application) : AndroidViewModel(application) {

    lateinit var string :String
    private val repository: FuncionRepository
    val allWords: LiveData<List<Funcion>>

    lateinit var estaObra: LiveData<List<Funcion>>

    init {
        val obraDao = FuncionRoomDatabase.getDatabase(application).funcionDao()
        repository = FuncionRepository(obraDao)
        allWords = repository.allWords
    }

    fun str(){
        estaObra = repository.estaObra(string)
    }
    fun deleteAll() = viewModelScope.launch(Dispatchers.IO) {
        repository.deleteAll()
    }

    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     */
    fun insert(obra: Funcion) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(obra)
    }
}