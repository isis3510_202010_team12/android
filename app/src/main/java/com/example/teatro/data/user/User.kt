package com.example.teatro.data.user

import androidx.room.*

@Entity(tableName = "usera_table")
data class User(
    @PrimaryKey @ColumnInfo(name = "email") val email: String
    , @ColumnInfo(name = "birthDate") val birthDate: String
    , @ColumnInfo(name = "preferences") val preferences: String
    , @ColumnInfo(name = "photo") val photo: String
    , @ColumnInfo(name = "name") val name: String
    , @ColumnInfo(name = "password") val password: String
)