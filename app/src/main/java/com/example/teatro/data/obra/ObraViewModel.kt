package com.example.teatro.data.obra

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ObraViewModel(application: Application) : AndroidViewModel(application) {

    lateinit var string: String
    private val repository: ObraRepository
    val allWords: LiveData<List<Obra>>
    lateinit var one: Obra

    lateinit var esteTeatro: LiveData<List<Obra>>

    init {
        val obraDao = ObraRoomDatabase.getDatabase(application).obraDao()
        repository = ObraRepository(obraDao)
        allWords = repository.allWords

    }

    fun str() {
        esteTeatro = repository.esteTeatro(string)
    }

    fun deleteAll() = viewModelScope.launch(Dispatchers.IO) {
        repository.deleteAll()
    }

    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     */
    fun insert(obra: Obra) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(obra)
    }

    fun getOne(text: String) : Obra{
        return repository.getOne(text)
    }
}