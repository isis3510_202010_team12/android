package com.example.teatro.data.funcion

import androidx.room.*

@Entity(tableName = "funcion_table")
data class Funcion(
    @PrimaryKey @ColumnInfo(name = "id") val id: String,
    @ColumnInfo(name = "asientos") val asientosDisponibles: Int,
    @ColumnInfo(name = "obra") val obra: String,
    @ColumnInfo(name = "fecha") val fecha: String
)