package com.example.teatro.data.funcion

import android.content.Context
import androidx.room.*

@Database(entities = [Funcion::class], version = 1, exportSchema = false)
public abstract class FuncionRoomDatabase : RoomDatabase() {

    abstract fun funcionDao(): FuncionDao
    companion object {
        @Volatile
        private var INSTANCE: FuncionRoomDatabase? = null

        fun getDatabase(
            context: Context): FuncionRoomDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    FuncionRoomDatabase::class.java,
                    "funcion_database"
                )
                    .build()
                INSTANCE = instance
                instance
            }
        }
    }
}
