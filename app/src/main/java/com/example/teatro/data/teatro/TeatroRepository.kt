package com.example.teatro.data.teatro

import androidx.lifecycle.LiveData

class TeatroRepository(private val teatroDao: TeatroDao) {

    val allWords: LiveData<List<Teatro>> = teatroDao.getTeatros()

    suspend fun deleteAll() {
        teatroDao.deleteAll()
    }

    suspend fun insert(teatro: Teatro) {
        teatroDao.insert(teatro)
    }
}