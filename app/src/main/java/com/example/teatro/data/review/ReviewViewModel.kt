package com.example.teatro.data.review

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ReviewViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: ReviewRepository
    val reviews: LiveData<List<Review>>

    init {
        val teatroDao = ReviewRoomDatabase.getDatabase(application).reviewDao()
        repository = ReviewRepository(teatroDao)
        reviews = repository.reviews
    }

    fun pending(): List<Review>{
        return repository.pending()
    }

    fun update(review: Review) = viewModelScope.launch(Dispatchers.IO) {
        repository.update(review)
    }

    fun deleteAll() = viewModelScope.launch {
        repository.deleteAll()
    }

    fun insert(obra: Review) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(obra)
    }
}