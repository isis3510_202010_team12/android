package com.example.teatro

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.RatingBar
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.teatro.data.review.Review
import com.example.teatro.data.review.ReviewViewModel
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.Error
import java.lang.Exception
import kotlin.reflect.typeOf

class ReviewActivity : AppCompatActivity() {

    private lateinit var reviewViewModel: ReviewViewModel
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private lateinit var firebaseFirestore: FirebaseFirestore

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_review)
        reviewViewModel = ViewModelProvider(this).get(ReviewViewModel::class.java)
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        firebaseFirestore = FirebaseFirestore.getInstance()
        firebaseAnalytics.setAnalyticsCollectionEnabled(true)

        val teatro = intent.extras?.get("teatro") as String
        val obra = intent.extras?.get("obra") as String

        val sharedPreferences =
            getSharedPreferences(getString(R.string.credentials), Context.MODE_PRIVATE)

        val user = sharedPreferences.getString("user", null)

        findViewById<Button>(R.id.button2).setOnClickListener {
            val value = ("" + findViewById<RatingBar>(R.id.ratingBar).rating).toDouble()
            reviewViewModel.update(Review(obra, user!!, value, teatro, true))
            val cm =
                applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork: NetworkInfo? = cm?.activeNetworkInfo
            val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
            if (isConnected) {
                val bundle = Bundle()
                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SHARE, bundle)

                val desd: ArrayList<Double> = ArrayList()

                GlobalScope.launch {
                    firebaseFirestore.collection("reviews").whereEqualTo("nameObra", obra)
                        .whereEqualTo("emailUser", user).get().addOnSuccessListener { result ->
                            result.forEach {
                                firebaseFirestore.collection("reviews").document(it.id)
                                    .update("value", value).addOnSuccessListener {
                                        reviewViewModel.update(
                                            Review(
                                                obra,
                                                user!!,
                                                value,
                                                teatro,
                                                false
                                            )
                                        )
                                        firebaseFirestore.collection("reviews")
                                            .whereEqualTo("nameObra", obra).get()
                                            .addOnSuccessListener { resultSet ->
                                                resultSet.forEach { result ->
                                                    try {
                                                        desd.add(result["value"] as Double)
                                                    } catch (e: Exception) {
                                                        // Ignore
                                                    }
                                                }
                                                Log.d("ANTES", "" + desd.average())
                                                firebaseFirestore.collection("obras").document(obra)
                                                    .update("score", desd.average())
                                                    .addOnSuccessListener {
                                                        Log.d("LOGRADO", "" + desd.average())
                                                    }.addOnFailureListener {
                                                        Log.e("SET", "failed")
                                                    }
                                            }.addOnFailureListener {
                                                Log.e("SET", "failed")
                                            }
                                    }
                            }
                        }.addOnFailureListener {
                            Log.e("SET", "failed")
                        }
                }
            }
            finish()
        }
    }
}
