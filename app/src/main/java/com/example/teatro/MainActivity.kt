package com.example.teatro

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.teatro.data.funcion.Funcion
import com.example.teatro.data.funcion.FuncionViewModel
import com.example.teatro.data.obra.Obra
import com.example.teatro.data.obra.ObraViewModel
import com.example.teatro.data.review.Review
import com.example.teatro.data.review.ReviewViewModel
import com.example.teatro.data.teatro.Teatro
import com.example.teatro.data.teatro.TeatroViewModel
import com.example.teatro.data.user.UserViewModel
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private lateinit var userViewModel: UserViewModel
    private lateinit var obraViewModel: ObraViewModel
    private lateinit var teatroViewModel: TeatroViewModel
    private lateinit var reviewViewModel: ReviewViewModel
    private lateinit var funcionViewModel: FuncionViewModel

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.my_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        auth = FirebaseAuth.getInstance()

        if (item.itemId == R.id.logout) {
            userViewModel.reset()
            auth.signOut()
            val sharedPreferences =
                this.getSharedPreferences(getString(R.string.credentials), Context.MODE_PRIVATE)
            sharedPreferences.edit().clear().apply()
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }

        firebaseAnalytics = FirebaseAnalytics.getInstance(this)

        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        userViewModel = ViewModelProvider(this).get(UserViewModel::class.java)
        obraViewModel = ViewModelProvider(this).get(ObraViewModel::class.java)
        teatroViewModel = ViewModelProvider(this).get(TeatroViewModel::class.java)
        reviewViewModel = ViewModelProvider(this).get(ReviewViewModel::class.java)
        funcionViewModel = ViewModelProvider(this).get(FuncionViewModel::class.java)

        GlobalScope.launch {
            val cm =
                applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork: NetworkInfo? = cm?.activeNetworkInfo
            val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
            if (isConnected) {
                loadData()
            }
        }

        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_list,
                R.id.navigation_map,
                R.id.navigation_camera,
                R.id.navigation_account
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        setupPermissions()
    }

    private fun setupPermissions() {
        val permission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), 0)
        }
    }

    public suspend fun loadData() = withContext(Dispatchers.IO){

        val db = FirebaseFirestore.getInstance()

        val sharedPreferences =
            getSharedPreferences(getString(R.string.credentials), Context.MODE_PRIVATE)

        val email = sharedPreferences.getString("user", null)

        val revs = reviewViewModel.pending()

        revs.forEach { xd ->
            val bundle = Bundle()
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SHARE, bundle)

            val desd: ArrayList<Double> = ArrayList()

            GlobalScope.launch {
                db.collection("reviews").whereEqualTo("nameObra", xd.nameObra)
                    .whereEqualTo("emailUser", xd.emailUser).get().addOnSuccessListener { result ->
                        result.forEach {
                            db.collection("reviews").document(it.id)
                                .update("value", xd.value).addOnSuccessListener {
                                    reviewViewModel.update(
                                        Review(
                                            xd.nameObra,
                                            xd.emailUser,
                                            xd.value,
                                            xd.teatro,
                                            false
                                        )
                                    )
                                    db.collection("reviews")
                                        .whereEqualTo("nameObra", xd.nameObra).get()
                                        .addOnSuccessListener { resultSet ->
                                            resultSet.forEach { result ->
                                                try {
                                                    desd.add(result["value"] as Double)
                                                } catch (e: Exception) {
                                                    //do nothing
                                                }
                                            }
                                            db.collection("obras").document(xd.nameObra)
                                                .update("score", desd.average())
                                                .addOnSuccessListener {
                                                    Log.d("LOGRADO", "" + desd.average())
                                                }.addOnFailureListener {
                                                    Log.e("SET", "failed")
                                                }
                                        }.addOnFailureListener {
                                            Log.e("SET", "failed")
                                        }
                                }
                        }
                    }.addOnFailureListener {
                        Log.e("SET", "failed")
                    }
            }
        }

        //Limpiar y obtener reviews
        reviewViewModel.deleteAll()
        db.collection("reviews").whereEqualTo("emailUser", email)
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    reviewViewModel.insert(
                        Review(
                            nameObra = document["nameObra"] as String,
                            emailUser = document["emailUser"] as String,
                            value = ("" + document["value"]).toDouble(),
                            teatro = document["teatro"] as String,
                            pending = false
                        )
                    )
                }
            }
            .addOnFailureListener { exception ->
                Log.w("COLLECTION", "Error getting documents.", exception)
            }

        //Limpiar y obtener obras
        obraViewModel.deleteAll()
        db.collection("obras")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    obraViewModel.insert(
                        Obra(
                            name = document.id,
                            image = document.data["image"] as String,
                            score = ("" + document.data["score"]).toDouble(),
                            description = document.data["description"] as String,
                            lugar = document.data["place"] as String,
                            escena = document.data["escena"] as String
                        )
                    )
                }
            }
            .addOnFailureListener { exception ->
                Log.w("COLLECTION", "Error getting documents.", exception)
            }

        //Limpiar y obtener teatros
        teatroViewModel.deleteAll()
        db.collection("teatros")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    teatroViewModel.insert(
                        Teatro(
                            name = document.id,
                            latitude = document["latitude"] as Double,
                            longitude = document["longitude"] as Double
                        )
                    )
                }
            }
            .addOnFailureListener { exception ->
                Log.w("COLLECTION", "Error getting documents.", exception)
            }

        //Limpiar y obtener funciones
        funcionViewModel.deleteAll()
        db.collection("horarios")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    funcionViewModel.insert(
                        Funcion(
                            id = document.id,
                            asientosDisponibles = Integer.parseInt("" + document["asientosDisponibles"] as Long),
                            fecha = document["horario"] as String,
                            obra = document["obra"] as String
                        )
                    )
                }
            }
            .addOnFailureListener { exception ->
                Log.w("COLLECTION", "Error getting documents.", exception)
            }
    }
}
