package com.example.teatro

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.teatro.data.funcion.Funcion

class FuncionListAdapter internal constructor(
    context: Context
) : RecyclerView.Adapter<FuncionListAdapter.WordViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var funciones = emptyList<Funcion>()

    inner class WordViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val date : TextView = itemView.findViewById(R.id.funcion_text)
        val seats : TextView = itemView.findViewById(R.id.seats_text)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WordViewHolder {
        val itemView = inflater.inflate(R.layout.funcion_recycler_view, parent, false)
        return WordViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: WordViewHolder, position: Int) {
        val current = funciones[position]
        holder.date.text = current.fecha
        holder.seats.text = current.asientosDisponibles.toString()
    }

    internal fun setWords(obras: List<Funcion>) {
        this.funciones = obras
        notifyDataSetChanged()
    }

    override fun getItemCount() = funciones.size
}