package com.example.teatro

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.teatro.auxiliar.IListener
import com.example.teatro.data.obra.Obra
import com.example.teatro.data.obra.ObraViewModel
import com.example.teatro.ui.obras.ObraListAdapter
import com.google.firebase.analytics.FirebaseAnalytics

class FilteredActivity : AppCompatActivity() {

    private lateinit var obraViewModel: ObraViewModel
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    inner class Escucha : IListener {
        override fun onClick(obra: Obra) {
            val bundle = Bundle()
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, obra.name)
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, obra.name)
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Obra")
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle)
            val intent = Intent(applicationContext, DetailActivity::class.java)
            intent.putExtra("name", obra.name)
            intent.putExtra("teatro", obra.lugar)
            intent.putExtra("image", obra.image)
            intent.putExtra("calificacion", "" + obra.score)
            intent.putExtra("descripcion", obra.description)
            intent.putExtra("escena", obra.escena)
            startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.obra_recicler_view)
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)

        val teatro = intent.extras?.get("teatro") as String
        supportActionBar!!.title = teatro

        val imageView: RecyclerView = findViewById<View>(R.id.list_recycler_view) as RecyclerView
        val adapter =
            ObraListAdapter(this, Escucha())

        imageView.adapter = adapter
        imageView.layoutManager = LinearLayoutManager(this)

        obraViewModel = ViewModelProvider(this).get(ObraViewModel::class.java)
            .apply { string = teatro }

        obraViewModel.str()

        obraViewModel.esteTeatro.observe(
            this,
            Observer { words -> words?.let { adapter.setWords(it) } })
    }
}