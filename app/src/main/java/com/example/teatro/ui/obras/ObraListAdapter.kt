package com.example.teatro.ui.obras

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.teatro.auxiliar.IListener
import com.example.teatro.R
import com.example.teatro.data.obra.Obra
import com.squareup.picasso.Picasso


class ObraListAdapter internal constructor(
    context: Context, private val itemClickListener: IListener
) : RecyclerView.Adapter<ObraListAdapter.WordViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var obras = emptyList<Obra>() // Cached copy of words

    inner class WordViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nName: TextView = itemView.findViewById(R.id.name)
        val description: TextView = itemView.findViewById(R.id.title)
        val score: TextView = itemView.findViewById(R.id.year)
        val img: ImageView = itemView.findViewById(R.id.item_image)
        fun bind(obra: Obra, clickListener: IListener) {
            itemView.setOnClickListener { clickListener.onClick(obra) }
            description.setOnClickListener { clickListener.onClick(obra) }
            score.setOnClickListener { clickListener.onClick(obra) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WordViewHolder {
        val itemView = inflater.inflate(R.layout.obra_card_layout, parent, false)
        return WordViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: WordViewHolder, position: Int) {
        val current = obras[position]
        holder.nName.text = current.name
        holder.description.text = current.description
        holder.score.text = current.score.toString()
        Picasso.get().load(current.image).into(holder.img)
        holder.bind(current, itemClickListener)
    }

    internal fun setWords(obras: List<Obra>) {
        this.obras = obras
        notifyDataSetChanged()
    }

    override fun getItemCount() = obras.size
}