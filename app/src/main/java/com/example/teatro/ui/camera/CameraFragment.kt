package com.example.teatro.ui.camera

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.budiyev.android.codescanner.*
import com.example.teatro.DetailActivity
import com.example.teatro.R
import com.example.teatro.data.obra.Obra
import com.example.teatro.data.obra.ObraViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class CameraFragment : Fragment() {

    private lateinit var codeScanner : CodeScanner
    private lateinit var obraViewModel: ObraViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_camera, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        obraViewModel = ViewModelProvider(this).get(ObraViewModel::class.java)
        val scannerView = view.findViewById<CodeScannerView>(R.id.scanner_view)

        codeScanner = CodeScanner(context!!, scannerView)

        // Parameters (default values)
        codeScanner.camera = CodeScanner.CAMERA_BACK // or CAMERA_FRONT or specific camera id
        codeScanner.formats = CodeScanner.ALL_FORMATS // list of type BarcodeFormat,
        // ex. listOf(BarcodeFormat.QR_CODE)
        codeScanner.autoFocusMode = AutoFocusMode.SAFE // or CONTINUOUS
        codeScanner.scanMode = ScanMode.SINGLE // or CONTINUOUS or PREVIEW
        codeScanner.isAutoFocusEnabled = true // Whether to enable auto focus or not
        codeScanner.isFlashEnabled = false // Whether to enable flash or not

        // Callbacks
        codeScanner.decodeCallback = DecodeCallback {
            GlobalScope.launch {
                xd(it.text)
            }
        }
        codeScanner.errorCallback = ErrorCallback { // or ErrorCallback.SUPPRESS
            Toast.makeText(context, "Camera initialization error: ${it.message}",Toast.LENGTH_LONG).show()
        }

        scannerView.setOnClickListener {
            codeScanner.startPreview()
        }
    }
    fun xd (text: String){
        val obra : Obra = obraViewModel.getOne(text)
        val intent = Intent(context, DetailActivity::class.java)
        intent.putExtra("name", obra.name)
        intent.putExtra("teatro", obra.lugar)
        intent.putExtra("image", obra.image)
        intent.putExtra("calificacion", "" + obra.score)
        intent.putExtra("descripcion", obra.description)
        intent.putExtra("escena", obra.escena)
        startActivity(intent)
    }

    override fun onResume() {
        super.onResume()
        codeScanner.startPreview()
    }

}