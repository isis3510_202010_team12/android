package com.example.teatro.ui.access

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.example.teatro.MainActivity
import com.example.teatro.R
import com.example.teatro.data.user.User
import com.example.teatro.data.user.UserViewModel
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*

class RegisterFragment : Fragment() {

    private lateinit var auth: FirebaseAuth
    private lateinit var userViewModel: UserViewModel
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        auth = FirebaseAuth.getInstance()
        firebaseAnalytics = FirebaseAnalytics.getInstance(context!!)
        val root = inflater.inflate(R.layout.fragment_register, container, false)
        val etDate = root.findViewById<EditText>(R.id.et_date)
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)
        etDate.setOnClickListener {
            val dpd = DatePickerDialog(
                root.context,
                DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                    val month1 = monthOfYear + 1
                    etDate.setText("$dayOfMonth/$month1/$year")
                },
                year,
                month,
                day
            )
            dpd.show()
        }
        root.findViewById<Button>(R.id.btn_register).setOnClickListener {
            val email = root.findViewById<EditText>(R.id.et_email_register).text.toString()
            val date = root.findViewById<EditText>(R.id.et_date).text.toString()
            val preferences = root.findViewById<EditText>(R.id.et_preferences).text.toString()
            val foto = root.findViewById<EditText>(R.id.et_photo).text.toString()
            val name = root.findViewById<EditText>(R.id.et_name).text.toString()
            val password1 = root.findViewById<EditText>(R.id.et_password_register).text.toString()
            val password2 = root.findViewById<EditText>(R.id.et_repassword_register).text.toString()
            val db = FirebaseFirestore.getInstance()
            if (password1 == password2 && email != "" && date != "" && preferences != "" && foto != "" && name != "" && password1 != "" && password2 != "") {
                auth.createUserWithEmailAndPassword(email, password1)
                    .addOnCompleteListener(activity!!) { task ->
                        if (task.isSuccessful) {
                            Log.d("REGISTER", "createUserWithEmail:success")
                            Toast.makeText(
                                root.context,
                                "Authentication success.",
                                Toast.LENGTH_SHORT
                            ).show()
                            val these = this
                            GlobalScope.launch {
                                val user = hashMapOf(
                                    "birthDate" to date,
                                    "preferences" to preferences,
                                    "photo" to foto,
                                    "name" to name
                                )
                                db.collection("users").document(email).set(user)
                                userViewModel =
                                    ViewModelProvider(these).get(UserViewModel::class.java)
                                userViewModel.insert(
                                    User(
                                        email,
                                        date,
                                        preferences,
                                        foto,
                                        name,
                                        password1
                                    )
                                )
                                val sharedPreferences = activity!!.getSharedPreferences(
                                    getString(R.string.credentials),
                                    Context.MODE_PRIVATE
                                )
                                sharedPreferences.edit().putString("user", email)
                                    .putString("pass", password1).apply()
                                val intent = Intent(root.context, MainActivity::class.java)
                                val bundle = Bundle()
                                bundle.putString(
                                    FirebaseAnalytics.Param.CONTENT,
                                    "Registro Exitoso"
                                )
                                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP, bundle)
                                startActivity(intent)
                                activity!!.finish()
                            }
                        } else {
                            Log.w("REGISTER", "createUserWithEmail:failure", task.exception)
                            Toast.makeText(
                                root.context,
                                "Authentication failed.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
            }
        }
        return root
    }

}
