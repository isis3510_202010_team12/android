package com.example.teatro.ui.account

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.teatro.R
import com.example.teatro.auxiliar.IListenerReviews
import com.example.teatro.data.review.Review

class ReviewListAdapter internal constructor(
    context: Context, private val rev : IListenerReviews
) : RecyclerView.Adapter<ReviewListAdapter.WordViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var obras = emptyList<Review>()

    inner class WordViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val obra: TextView = itemView.findViewById(R.id.obra_text)
        val teatro: TextView = itemView.findViewById(R.id.teatro_text)
        val calif: TextView = itemView.findViewById(R.id.year)
        val star: View = itemView.findViewById(R.id.calificacion)
        val bot: View = itemView.findViewById(R.id.boton_calif)
        fun bind(review: Review, clickListener: IListenerReviews) {
            bot.setOnClickListener { clickListener.onClick(review) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WordViewHolder {
        val itemView = inflater.inflate(R.layout.review_item_layout, parent, false)
        return WordViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: WordViewHolder, position: Int) {
        val current = obras[position]
        if (current.value == -1.0){
            holder.star.visibility = View.GONE
            holder.bot.visibility = View.VISIBLE
        }
        else{
            val cur = current.value
            holder.calif.text = "$cur"
        }
        holder.obra.text = current.nameObra
        holder.teatro.text = current.teatro
        holder.bind(current, rev)
    }

    internal fun setWords(obras: List<Review>) {
        this.obras = obras
        notifyDataSetChanged()
    }

    override fun getItemCount() = obras.size
}