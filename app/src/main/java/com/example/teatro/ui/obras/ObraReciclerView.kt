package com.example.teatro.ui.obras

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.teatro.DetailActivity
import com.example.teatro.MainActivity
import com.example.teatro.auxiliar.IListener
import com.example.teatro.R
import com.example.teatro.data.obra.Obra
import com.example.teatro.data.obra.ObraViewModel
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ObraReciclerView : Fragment() {

    private lateinit var obraViewModel: ObraViewModel

    inner class Escucha : IListener {
        override fun onClick(obra: Obra) {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra("name", obra.name)
            intent.putExtra("teatro", obra.lugar)
            intent.putExtra("image", obra.image)
            intent.putExtra("calificacion", "" + obra.score)
            intent.putExtra("descripcion", obra.description)
            intent.putExtra("escena", obra.escena)
            startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.obra_recicler_view, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val imageView: RecyclerView =
            getView()!!.findViewById<View>(R.id.list_recycler_view) as RecyclerView
        val adapter = ObraListAdapter(
            getView()!!.context,
            Escucha()
        )
        imageView.adapter = adapter
        imageView.layoutManager = LinearLayoutManager(activity)
        obraViewModel = ViewModelProvider(this).get(ObraViewModel::class.java)
        obraViewModel.allWords.observe(
            viewLifecycleOwner,
            Observer { words -> words?.let { adapter.setWords(it) } })
    }
}