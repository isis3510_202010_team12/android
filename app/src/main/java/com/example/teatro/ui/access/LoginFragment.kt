package com.example.teatro.ui.access

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.example.teatro.MainActivity
import com.example.teatro.R
import com.example.teatro.data.user.User
import com.example.teatro.data.user.UserViewModel
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class LoginFragment : Fragment() {

    private lateinit var auth: FirebaseAuth
    private lateinit var userViewModel: UserViewModel
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_login, container, false)
        auth = FirebaseAuth.getInstance()
        firebaseAnalytics = FirebaseAnalytics.getInstance(context!!)

        root.findViewById<Button>(R.id.btn_login).setOnClickListener {

            val email = root.findViewById<EditText>(R.id.et_email).text.toString()
            val password = root.findViewById<EditText>(R.id.et_password).text.toString()
            val db = FirebaseFirestore.getInstance()

            auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(activity!!) { task ->
                    if (task.isSuccessful) {
                        val these = this
                        GlobalScope.launch {
                            db.collection("users").document(email).get()
                                .addOnSuccessListener { document ->
                                    if (document != null) {
                                        val intent = Intent(root.context, MainActivity::class.java)
                                        startActivity(intent)
                                        userViewModel =
                                            ViewModelProvider(these).get(UserViewModel::class.java)
                                        userViewModel.insert(
                                            User(
                                                email,
                                                document["birthDate"] as String,
                                                document["preferences"] as String,
                                                document["photo"] as String,
                                                document["name"] as String,
                                                password
                                            )
                                        )
                                        val sharedPreferences = activity!!.getSharedPreferences(
                                            getString(R.string.credentials),
                                            Context.MODE_PRIVATE
                                        )
                                        sharedPreferences.edit().putString("user", email)
                                            .putString("pass", password).apply()
                                        val bundle = Bundle()
                                        bundle.putString(
                                            FirebaseAnalytics.Param.CONTENT,
                                            "Logueo Exitoso"
                                        )
                                        firebaseAnalytics.logEvent(
                                            FirebaseAnalytics.Event.LOGIN,
                                            bundle
                                        )
                                        activity!!.finish()
                                    } else {
                                        Toast.makeText(
                                            context, "Authentication failed.",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                    }
                                }
                        }
                    } else {
                        // If sign in fails, display a message to the user.
                        Toast.makeText(
                            context, "Authentication failed.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
        }
        return root
    }

}
