package com.example.teatro.ui.map

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.teatro.FilteredActivity
import com.example.teatro.MainActivity
import com.example.teatro.R
import com.example.teatro.data.teatro.TeatroViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MapFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private lateinit var mMap: GoogleMap

    private lateinit var teatroViewModel: TeatroViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        GlobalScope.launch {
            val cm =
                activity!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork: NetworkInfo? = cm?.activeNetworkInfo
            val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
            if (isConnected) {
                (activity!! as MainActivity).loadData()
            }
        }
        val root = inflater.inflate(R.layout.fragment_map, container, false)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map1) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
        return root
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val bogota = LatLng(4.624335, -74.063644)

        mMap.setOnMarkerClickListener(this)

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(bogota, 12.0f))
        teatroViewModel = ViewModelProvider(this).get(TeatroViewModel::class.java)
        teatroViewModel.teatros.observe(
            viewLifecycleOwner,
            Observer { words ->
                words.forEach { word ->
                    mMap.addMarker(
                        MarkerOptions().position(LatLng(word.latitude, word.longitude))
                            .title(word.name)
                    )
                }
            })
    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        val intent = Intent(context, FilteredActivity::class.java)
        intent.putExtra("teatro", p0?.title)
        startActivity(intent)
        return true
    }

}