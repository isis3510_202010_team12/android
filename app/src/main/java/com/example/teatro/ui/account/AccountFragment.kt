package com.example.teatro.ui.account

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.example.teatro.auxiliar.CircleTransform
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.teatro.MainActivity
import com.example.teatro.R
import com.example.teatro.ReviewActivity
import com.example.teatro.auxiliar.IListenerReviews
import com.example.teatro.data.review.Review
import com.example.teatro.data.review.ReviewViewModel
import com.example.teatro.data.user.UserViewModel
import com.squareup.picasso.Picasso
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class AccountFragment : Fragment() {

    private lateinit var accountViewModel: UserViewModel
    private lateinit var reviewViewModel: ReviewViewModel

    inner class Escucha : IListenerReviews {
        override fun onClick(obra: Review) {
            val intent = Intent(context, ReviewActivity::class.java)
            intent.putExtra("teatro", obra.teatro)
            intent.putExtra("image", obra.emailUser)
            intent.putExtra("obra", "" + obra.nameObra)
            startActivityForResult(intent, 3)

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        GlobalScope.launch {
            val cm =
                activity!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork: NetworkInfo? = cm?.activeNetworkInfo
            val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
            if (isConnected) {
                (activity!! as MainActivity).loadData()
            }
        }
        return inflater.inflate(R.layout.fragment_account, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        accountViewModel = ViewModelProvider(this).get(UserViewModel::class.java)
        accountViewModel.users.observe(viewLifecycleOwner, Observer { user ->
            user?.let {
                val image: ImageView = view.findViewById(R.id.imageView)
                Picasso.get().load(user.photo).transform(CircleTransform()).resize(400, 400)
                    .centerCrop().into(image)
                val name: TextView = view.findViewById(R.id.text_account)
                name.text = user.name
                val date: AppCompatTextView = view.findViewById(R.id.text_date)
                date.text = user.birthDate
                val preferences: TextView = view.findViewById(R.id.text_preferences)
                preferences.text = user.preferences
            }
        })

        reviewViewModel = ViewModelProvider(this).get(ReviewViewModel::class.java)

        val imageView: RecyclerView =
            view.findViewById<View>(R.id.reviews) as RecyclerView
        val adapter = ReviewListAdapter(
            view.context, Escucha()
        )
        imageView.adapter = adapter
        imageView.layoutManager = LinearLayoutManager(activity)
        reviewViewModel.reviews.observe(
            viewLifecycleOwner,
            Observer { words -> words?.let { adapter.setWords(it) } })
    }
}