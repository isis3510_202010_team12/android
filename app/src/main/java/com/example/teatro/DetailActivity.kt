package com.example.teatro

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.teatro.data.funcion.FuncionViewModel
import com.squareup.picasso.Picasso

class DetailActivity : AppCompatActivity() {

    private lateinit var funcionViewModel :FuncionViewModel

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_obra_detail)
        val teatro = intent.extras?.get("name") as String
        supportActionBar!!.title = teatro

        Picasso.get().load(intent.extras?.get("image") as String)
            .into(findViewById<ImageView>(R.id.imageView3))

        findViewById<TextView>(R.id.titulo2).text = teatro
        findViewById<TextView>(R.id.nombre_teatro).text = intent.extras?.get("teatro") as String
        findViewById<TextView>(R.id.year).text = intent.extras?.get("calificacion") as String
        findViewById<TextView>(R.id.textView6).text = intent.extras?.get("descripcion") as String
        findViewById<TextView>(R.id.textView7).text = intent.extras?.get("escena") as String

        funcionViewModel = ViewModelProvider(this).get(FuncionViewModel::class.java)

        funcionViewModel.apply { string = teatro }

        funcionViewModel.str()

        findViewById<Button>(R.id.button).setOnClickListener {
            val inte = Intent(this, BuyActivity::class.java)
            inte.putExtras(intent)
            startActivity(inte)
        }

        val imageView: RecyclerView =
            findViewById<View>(R.id.reviews) as RecyclerView
        val adapter = FuncionListAdapter(
            this
        )
        imageView.adapter = adapter
        imageView.layoutManager = LinearLayoutManager(this)
        funcionViewModel.estaObra.observe(
            this,
            Observer { words -> words?.let { adapter.setWords(it) } })
    }
}