package com.example.teatro

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.teatro.ui.access.LoginFragment
import com.example.teatro.ui.access.RegisterFragment

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme_NoActionBar)
        super.onCreate(savedInstanceState)

        val sharedPreferences =
            getSharedPreferences(getString(R.string.credentials), Context.MODE_PRIVATE)

        val user = sharedPreferences.getString("user", null)

        if (user == null) {
            setContentView(R.layout.activity_login)

            val viewPager = findViewById<ViewPager>(R.id.viewPager)

            val pagerAdapter = AuthenticationPagerAdapter(supportFragmentManager)
            pagerAdapter.addFragmet(LoginFragment())
            pagerAdapter.addFragmet(RegisterFragment())
            viewPager.adapter = pagerAdapter
        } else {
            val intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    internal class AuthenticationPagerAdapter(fm: FragmentManager) :
        FragmentPagerAdapter(fm) {
        private val fragmentList: ArrayList<Fragment> = ArrayList()
        override fun getItem(i: Int): Fragment {
            return fragmentList[i]
        }

        override fun getCount(): Int {
            return fragmentList.size
        }

        fun addFragmet(fragment: Fragment) {
            fragmentList.add(fragment)
        }
    }
}